# React/Rails Monorepo boilerplate

This monorepo is intended as a boilterplate repository that you can fork that has React/Rails setup by default

## Installation

Run `yarn install` in the root folder

## Usage

Run `yarn start` in the root folder